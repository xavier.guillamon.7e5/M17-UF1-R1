using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FireBall : MonoBehaviour
{
    Scene currentScene;
    string sceneName;
    // Start is called before the first frame update
    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnHit()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            GetComponent<Animator>().SetTrigger("Explosion");
            var rb = GetComponent<Rigidbody2D>();
            rb.gravityScale = 0f;
            rb.velocity = new Vector2(0, 0);
            GameManager.Instance.IncreaseFireBallCount();
            Debug.Log(this.name + " " + GameManager.Instance.GetFireBallCounter());
        }
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("HitBox"))
        {
            Debug.Log("Hola no?");

            if (sceneName == "GameMode2")
            {
                Debug.Log("Primer if");
                PlayerPrefs.SetInt("GameMode", 2);
                Destroy(collision.gameObject);
                SceneManager.LoadScene("GameOver");
            }
            else if(sceneName == "GameMode1")
            {
                Debug.Log("Segundo if");
                PlayerPrefs.SetInt("GameMode", 1);
                Destroy(collision.gameObject);
                SceneManager.LoadScene("GameOver");

            }
        }
    }
}
