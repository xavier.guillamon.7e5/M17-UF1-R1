using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _fireball;
    [SerializeField] private Text _timer;
    private float _whenItSpawns;
    private float _howMuchTimeToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        _whenItSpawns = 1;
        _howMuchTimeToSpawn = _whenItSpawns;
    }

    // Update is called once per frame
    void Update()
    {
        if(_timer.text == "30") _whenItSpawns = 0.5f;
        if(_timer.text == "05") _whenItSpawns = 0.25f;
        
        _howMuchTimeToSpawn -= Time.deltaTime;
        if( _howMuchTimeToSpawn < 0 )
        {
            Instantiate(_fireball, new Vector3(Random.Range(-9f, 9f), transform.position.y, transform.position.z), new Quaternion(0, 0, 0, 0));
            _howMuchTimeToSpawn = _whenItSpawns;
        }
    }
}
