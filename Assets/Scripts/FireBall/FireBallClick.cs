using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallClick : MonoBehaviour
{
    [SerializeField] GameObject fireball;
    public float FireballCooldown;
    public float indexCooldown;

    void Start()
    {
        indexCooldown = 0;
    }
    // Update is called once per frame
    void Update()
    {
        indexCooldown -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            indexCooldown -= Time.deltaTime;
            if (indexCooldown < 0)
            {
                Vector2 spawnPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 600, 0));
                GameObject go = Instantiate(fireball, spawnPosition, Quaternion.identity);
                indexCooldown = FireballCooldown;
            }
            
        }
    }
}
