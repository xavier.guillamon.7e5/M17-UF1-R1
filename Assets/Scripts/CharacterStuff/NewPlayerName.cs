using System;
using UnityEngine;
using UnityEngine.UI;

public class NewPlayerName : MonoBehaviour
{
    public GameObject saveName;
    public GameObject newName;

    public void Awake()
    {
        newName = GameObject.Find("playerNewName");
        saveName = GameObject.Find("playerName");
    }

    // Start is called before the first frame update
    void Start()
    {
        newName.GetComponent<Text>().text = saveName.GetComponent<Text>().text;
    }
}
