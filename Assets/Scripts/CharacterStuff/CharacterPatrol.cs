using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPatrol : MonoBehaviour
{
    /*public float speed;
    public float distanceLaser;

    private bool movingRight = true;

    public Transform groundDetection;
    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distanceLaser);
        Debug.Log(groundInfo);
        if (groundInfo.collider == false)
        {
            Debug.Log("Primer IF");
            if (movingRight == true)
            {
                Debug.Log("Segundo IF");
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            } else
            {
                Debug.Log("Tercer IF");
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
    }*/

    public float moveSpeed = 1f;
    new Rigidbody2D rigidbody2D;
    BoxCollider2D boxCollider;

    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        boxCollider= GetComponent<BoxCollider2D>();
        GetComponent<Animator>().SetBool(("isRunning"), true);
    }
    void Update()
    {
        if (IsFacingRight())
        {
            rigidbody2D.velocity = new Vector2(moveSpeed, 0f);
        }
        else
        {
            rigidbody2D.velocity = new Vector2(-moveSpeed, 0f);
        }
    }
    private bool IsFacingRight()
    {
        return transform.localScale.x > Mathf.Epsilon;
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (!collider.gameObject.CompareTag("Fireball"))
        {
            transform.localScale = new Vector2(-Mathf.Sign(rigidbody2D.velocity.x), transform.localScale.y);
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.CompareTag("Fireball")) 
        {
            transform.localScale = new Vector2(-Mathf.Sign(rigidbody2D.velocity.x), transform.localScale.y);
        }
    }

}
