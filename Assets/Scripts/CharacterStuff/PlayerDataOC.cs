using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows;
using Input = UnityEngine.Input;

public class PlayerDataOC : MonoBehaviour
{
    public float jumpForce;
    public float speed;

    [SerializeField] private LayerMask platformLayerMask;
    private Rigidbody2D _rb;
    private BoxCollider2D _bc;
    private Animator _anim;

    public Transform groundPos;
    public float checkRadius;
    public LayerMask whatIsGround;

    private float jumpTimeCounter;
    public float jumpTime;
    private bool isJumping;
    private bool doubleJump;

    
    // Start is called before the first frame update
    void Start()
    {
        _anim = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
        _bc = GetComponent<BoxCollider2D>();
        
    }

    // Update is called once per frame
    void Update()
    {

        if (IsGrounded() == true && Input.GetKeyDown(KeyCode.Space))
        {
            _anim.SetTrigger("takeOf");
            isJumping = true;
            jumpTimeCounter = jumpTime;
            _rb.velocity = Vector2.up * jumpForce;
        }

        if (IsGrounded() == true)
        {
            doubleJump = false;
            _anim.SetBool("isJumping", false); 
        }
        else _anim.SetBool("isJumping", true);

        if(isJumping == true && Input.GetKey(KeyCode.Space))
        {
            if (jumpTimeCounter > 0)
            {
                _rb.velocity = Vector2.up * jumpForce;
                jumpTimeCounter -= Time.deltaTime;
            }
            else
                isJumping = false;
        }
        if (Input.GetKeyUp(KeyCode.Space)) isJumping = false;

        if(IsGrounded() == false && doubleJump == false && Input.GetKeyDown(KeyCode.Space))
        {
            isJumping = true;
            doubleJump = true;
            isJumping = true;
            _anim.SetTrigger("takeOf");
            jumpTimeCounter = jumpTime;
            _rb.velocity = Vector2.up * jumpForce;
        }

        float moveInput = Input.GetAxisRaw("Horizontal");
        _rb.velocity = new Vector2(moveInput * speed, _rb.velocity.y);

        if (moveInput == 0) _anim.SetBool("isRunning", false);
        else _anim.SetBool("isRunning", true);

        if(moveInput < 0) transform.eulerAngles = new Vector3(0, 180, 0);
        else if (moveInput > 0) transform.eulerAngles = new Vector3(0, 0, 0);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("BlastZone"))
        {
            Debug.Log("Hola");
            Destroy(collision.gameObject);
            SceneManager.LoadScene("GameOver");
        }
    }

    private bool IsGrounded()
    {
        float extraHeightText = 1f;
        RaycastHit2D raycastHit = Physics2D.BoxCast(_bc.bounds.center, _bc.bounds.size, 0f, Vector2.down, extraHeightText, platformLayerMask);
        return raycastHit.collider != null;
    }

}
