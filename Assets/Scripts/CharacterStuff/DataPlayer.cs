using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DataPlayer : MonoBehaviour
{
    public new string name;
    public float height;
    public float weight;
    public float speed;
    public float distance;
    public float _amplifier = 0f;
    public float currentLife;
    public Animation[] Animation;

    public Collider2D Ground;
    private float _timeCounterQuiet = 0f;
    private const float _timeBetweenWalking = 2f;
    private Vector3 _velocity;
    bool flipped = false;

    void Start()
    {
        _velocity = new Vector3(speed/weight, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }
    void Movement()
    {
        _timeCounterQuiet += Time.deltaTime;
        if (_timeCounterQuiet > _timeBetweenWalking)
        {
            transform.position = _velocity;
            _timeCounterQuiet = 0f;
            GetComponent<Animator>().SetBool(("isRunning"), true);
            flipped = !flipped;
            this.transform.rotation = Quaternion.Euler(new Vector3(0f, flipped ? 180f : 0f, 0f));
        }
    }
}
