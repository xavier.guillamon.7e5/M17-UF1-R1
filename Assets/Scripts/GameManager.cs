using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    [SerializeField]
    private int _fireBallCounter;
    public float InitialPlayerLife = 3f;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager is NULL");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this) Destroy(this.gameObject);
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().ToString());

    }
    public void IncreaseFireBallCount()
    {
        _fireBallCounter++;
    }
    public int GetFireBallCounter()
    {
        return _fireBallCounter;
    }
}
