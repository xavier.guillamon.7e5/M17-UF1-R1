using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerMode2 : MonoBehaviour
{
    public Text timerText;
    public float seconds = 60f;
    private float _indexSeconds = 0f;
    // Start is called before the first frame update
    void Start()
    {
        timerText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        seconds -= Time.deltaTime;
        if (_indexSeconds < seconds)
        {
            if (seconds <= 9.50f) timerText.text = "0" + Mathf.Abs(seconds).ToString("f0");
            else timerText.text = seconds.ToString("f0");
        }
        if (seconds < 0)
        {
            PlayerPrefs.SetInt("GameMode", 2);
            SceneManager.LoadScene("GameOver");
        }
    }
}
