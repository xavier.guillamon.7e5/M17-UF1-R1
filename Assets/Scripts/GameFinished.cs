using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameFinished : MonoBehaviour
{
    void Start()
    {
    }
    public int GetInt(string name)
    {
        return PlayerPrefs.GetInt(name);
    }
    public void RestartButton()
    {
        if(GetInt("GameMode") == 1)
        {
            SceneManager.LoadScene("GameMode1");
        } else
        {
            SceneManager.LoadScene("GameMode2");
        }

    }
    public void ExitButton()
    {
        SceneManager.LoadScene("GameMenu");
    }
}
