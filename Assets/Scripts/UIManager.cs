using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    private GameObject _nameInput; // Input donde escribimos
    private GameObject _nameOnText; // 
    private InputField _input;
    private Text _nameOnInput;
    public HelpOverScreen HelpOverScreen;
    // Start is called before the first frame update
    void Start()
    {
        _nameOnText = GameObject.Find("playerName");
        
        _nameInput = GameObject.Find("NameInput");
    }
    
    public void LoadGameOne()
    {
        _input = _nameInput.GetComponent<InputField>();
                
        _nameOnInput = _nameOnText.GetComponent<Text>();

        _nameOnInput.text = _input.text;
        
        SceneManager.LoadScene("GameMode1");
    }
    public void LoadGameTwo()
    {
        _input = _nameInput.GetComponent<InputField>();

        _nameOnInput = _nameOnText.GetComponent<Text>();

        _nameOnInput.text = _input.text;

        SceneManager.LoadScene("GameMode2");
    }
    public void HelpButton()
    {
        Instantiate(HelpOverScreen);
        HelpOverScreen.Setup();
    }
}
