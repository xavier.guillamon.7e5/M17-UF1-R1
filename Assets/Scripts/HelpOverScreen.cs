using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpOverScreen : MonoBehaviour
{
    public void Setup()
    {
        gameObject.SetActive(true);
    }
    public void BackMenu()
    {
        gameObject.SetActive(false);
    }
}
